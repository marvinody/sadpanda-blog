templates/post.tmpl
Getting around to making a blog (soon™)
After having MFSSG available for a while, I finally bothered to link a repo with netlify and the binary. Turns out just having <code>MFSSG</code> isn't enough for a custom build command and needed <code>./MFSSG</code> which I didn't think would be necessary since you're putting a custom command...?
Anyway, it seems to work fine for the most part except I'm having some issues currently with the domain not redirecting but that's not Netlify's fault.
Aside from those two slight issues, my largest concern isn't the software or the services, but myself. I'm not sure of the writing style I should use or how to make posts enticing or knowledgeable. I know that it isn't something you get better at overnight and it takes time, but that doesn't make it any easier to start. I know I would like to have a 'stats' day to publish some data from my sites each month so I can look back and see how they've grown.
It's very difficult for me to come up with ideas for what to write on the first one since this is pretty generic and just a test post essentially, so I'll leave it at that.
