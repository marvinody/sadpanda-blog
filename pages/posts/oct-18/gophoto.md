templates/post.tmpl
GoPhoto
I'll be giving a (hopefully) short explanation on the reasoning behind my ideas in making [GoPhoto](https://bitbucket.org/marvinody/gophoto/src/master/), a learning excercise in image manipulation with Golang.

# Yet Another Image Program?
Well, I never really made a nice one with effects and I wanted to play with some images (particularly fumo images), so that was the reasoning behind this project. As usual, I'm not really a fan of using libraries until I understand what they're doing or absolutely necessary, so (I believe) there should not be any included in this project.
In fact, the only library that's being used is a kd\-tree structure I wrote myself which is available [here](https://bitbucket.org/marvinody/kdtree/src/master/). It's not really a full-fledged kd\-tree library (no deletes at the time of this writing), but it can handle bulk inserts and efficient search which is all I needed from it for one of the effects.
One of the goals I had when starting this was to have it be used in the terminal and take the image input with stdin instead of taking a filename. I've wanted to make something of that sort for a while but my attempt to get something simple and similar working in python failed until I thought about doing it in Go much later.

# Usage
Once you have the binary in `/usr/local/bin/` or somewhere in your path, assuming you have an image called `shion.png` [[Shion from @yumemimeme]](/static/images/gophoto/shion.png) in your current directory, you can try `gophoto < shion.png > out.png` or `cat shion.png | gophoto > out.png` which should just copy the image.

Once that's working and you want to apply effects, simply pass the effect name, semicolon delimited if multiple, and you'll get your image.

## Examples
Some of the effects I've grown fond of:
### Crystallize
`gophoto crystallize < shion.png > crystal_shion.png`
[[Crystal Shion]](/static/images/gophoto/crystal_shion.png)
### Glasstile
`gophoto glasstile < shion.png > glass_shion.png`
[[Glass Shion]](/static/images/gophoto/glass_shion.png)
### Glasstile -> maxrgb
`gophoto "glasstile;maxrgb" < shion.png > weird_shion.png`
[[Weird Shion]](/static/images/gophoto/weird_shion.png)

# Filters
I've mainly looked around gimp's own source for cool filters that were doable. I even stepped into some Gimp IRC channel to ask about the reason behind a few weird lines (which they were fairly friendly on explaining).
In no particular order:
## Edge (sobel)
  Take two convolution matrices (one each for horizontal and vertical), apply them and for each channel, square the values, sum them and take the square root. Check [wiki page](https://en.wikipedia.org/wiki/Sobel_operator) for a better understanding.
## Maxrgb
  For each pixel, find the greatest value(s) of RGB and drop the rest of the channels. That's it.
## Glasstile
  This is one I essentially copied from Gimp's source. My understanding is that it's sampling some area into smaller copies of itself which replace the larger area in the final picture.
## Crystallize
  Probably my favorite because of how it came to me. I saw a mosaic like picture one day and the polygons looked Voronoi-like to me so I thought if it was that simple. Apparently, yes. This picks random pixels within set squares and then does a Voronoi representation using those. I ended up writing a limited kd tree library for this so I could practice more go!
## Duotone
  A sepia-like effect that maps the hue of HSV values into a gradient of two colors. I'm not entirely satisfied with the defaults I've chosen here, but it's fine for now.
## Pixelize
  Take constant rectangles of arbitrary sizes, get the average pixel color using unweighted RGB, and then all rectangles get that color.
## Predator
  A failed attempt to copy a composition of effects from Gimp's source. I'm not entirely sure what part fails, but I get very different results compared to running the same effect in Gimp.

# What next?
I think I'll add more filters as time goes on when I see something cool. Like with most things I make, it's not for a specific purpose other than to learn and have fun with it, so I give no guarantees on me coming back to it.
