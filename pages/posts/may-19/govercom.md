templates/post.tmpl
Low Memory JPEG Appending (Govercom)
Any reader of Korean webcomics knows how annoying it is when you get to the end of an image and barely read 2 panels. How do you fix that? By appending all of them into one (or more) super image(s).

# This doesn't really sound like a problem
And you're probably right. But a friend asked me to solve this so I tried. The 'formal' question was, given a folder of images, vertically combine them into one or more images.
My first attempt was to use imagemagick's tools and some simple bash scripting. This worked fine until it started crashing because of some out of memory error with imagemagick. I fixed it with changing some default config options but then I realized it would be great if I could just give the script to my friend and have him run it. He's not a unix person so no unix tools.

# Enter Go
Golang lets me compile some code into a binary which I can simply give to him and let him run it. I was familiar with the [draw](https://golang.org/pkg/image/draw) package which, after calculating bounds, let me do the appending pretty nicely.

## So why the post?
First some basic image knowledge. Having a single (RGBA) pixel in memory means about 4 bytes. R,G,B and A can be represented each with a single byte (0-255 range).
Now, if you have an image 300x300 pixels (a smallish thumb), and you load it into memory (inefficiently but go with it), it would take 90,000 (300x300) pixels * 4 bytes per pixel = 360,000 bytes. Only 360KB you might say. And you would be correct. But this is only for a wimpy 300x300.
What is this, a webcomic for ants?

## Does this really have to be optimized?
Yes.
Normal widths can be anywhere from 700px-1200px. Now remember the point is to combine all the images in a webtoon, pushing us to the maximum of the JPEG world height-wise. JPEG standard dictates that the maximum in either dimension is 64,000, meaning a max memory usage of 1200x64000*4 bytes which is approx 300MB. Per maximum scale image of which there can be multiple. This will work, but not too well for lower memory computers.
So the original golang implementation loaded all the images into memory (which was an obvious unneccesary drain). The first optimization was simply to chunk the images and load them only when working on that panel. This brought down memory to about 200MB-300MB from a usual 4,000MB amount for a fully loaded chapter. Remember that I have to keep the whole panel until I'm done working on it.
I was ok with that for a while until I got a complaint about slow speeds. I sacrificed high memory usage and high speed for low memory usage but lower speed. Because it can't load them and work concurrently, it has to do one by one and takes some more time.
## What more can you do?
I'm glad you asked random stranger. I had read the JPEG spec previously and learned that it processes images in chunks and writes to the end continually. JPEG is also super compressed compared to the raw image data so once written, I could just close the original image and keep the compressed data until done. And that's exactly what it does. It chunks the images into sections and now it can work on those concurrently. They're all disjoint sets and can be updated simultaneously and since each JPEG panel requires little memory, we're down to high speed and low memory usage!

## Can you do anything else?
I'm not entirely sure it's always doable, but I feel like you can go even more concurrent. An issue I imagine is that you would have to buffer writes to a certain offset and crop images in a particular way which could be annoying to the reader. Is this worth the tradeoff? Probably not at the moment. The added complexity and speed aren't particularly needed.

